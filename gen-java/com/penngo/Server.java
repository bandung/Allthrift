package com.penngo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.penngo.FaceService.Iface;
import com.penngo.LoginService;
import com.penngo.LoginServiceImpl;
import com.penngo.RegisterService;
import com.penngo.RegisterServiceImpl;
import com.penngo.client.PythonClient;
import com.penngo.client.TestService.Processor;
import com.penngo.word.WordToPdf;

import net.sf.json.JSONArray;
 
public class Server {
	
	//注册启动多个Service
	private void TMstart() {
		try {
			TServerSocket serverTransport = new TServerSocket(8848);
			// 用户登录
			LoginService.Processor loginProcessor = new LoginService.Processor(
					new LoginServiceImpl());
			
			FaceService.Processor faceProcessor=new FaceService.Processor(new FaceServiceImpl());
			// 用户注册
			RegisterService.Processor registerProcessor = new RegisterService.Processor(
					new RegisterServiceImpl());
			
			
			 
			TMultiplexedProcessor processor = new TMultiplexedProcessor();
			
			processor.registerProcessor("LoginService", loginProcessor);
			processor.registerProcessor("RegisterService", registerProcessor);
			processor.registerProcessor("FaceService", faceProcessor);
			TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(
					serverTransport).processor(processor));
			System.out.println("Starting server on port 8848 ...");
			server.serve();
		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**TSimpleServer的工作模式只有一个工作线程，循环监听新请求的到来并完成对请求的处理，只用于测试
	TSimpleServer的工作模式采用最简单的阻塞IO，实现方法简洁明了，便于理解，但是一次只能接收和处理一个socket连接，
	效率比较低，主要用于演示Thrift的工作过程，在实际开发过程中很少用到它。
	 */

	public void TSstart() {
		 TServerSocket serverSocket = null;
		try {
			serverSocket = new TServerSocket(8848);
		} catch (TTransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 FaceService.Iface faceService = new FaceServiceImpl();
		 FaceService.Processor<Iface> processor = new FaceService.Processor<Iface>(faceService);
		 TServer.Args serverParams=new TServer.Args(serverSocket); 
		 serverParams.protocolFactory(new TBinaryProtocol.Factory());
		 serverParams.processor(processor); 
		 TServer server=new TSimpleServer(serverParams); //简单的单线程服务模型，常用于测试 
		 
		 System.out.println("Starting server on port 8848 ...");
		 server.serve();
		 
	}
	
	
 
	public static void main(String args[]) {
		Server srv = new Server();
		srv.TMstart();
		
//		new PythonClient().python();


		
//		String str = "{\"from\":\"F:\\\\C\\\\Thrift\\\\gen-java\\\\php.docx\",\"to\":\"F:\\\\C\\\\Thrift\\\\gen-java\\\\5c4d5a272f907pdf\"}";
		String str = "[{'from':'kevin','to':25},{'from':'cissy','to':24}]";
//		String str = "[{\"name\":\"array\",\"id\":123456,\"date\":\"2013-4-13 12:36:54\"},{\"name\":\"tom\",\"id\":123,\"date\":\"2013-4-13 12:36:54\"}]";
		//创建一个Gson对象
//		Gson gson = new Gson();
//		//创建一个JsonParser
//		JsonParser parser = new JsonParser();
//		//通过JsonParser对象可以把json格式的字符串解析成一个JsonElement对象
//		JsonElement el = parser.parse(str);

		//把JsonElement对象转换成JsonArray
//		JsonArray jsonArray = null;
//		if(el.isJsonArray()){
//		jsonArray = el.getAsJsonArray();
//		}
//
//		System.out.println(jsonArray);
//		
//		Product product = null;
//		Iterator it = jsonArray.iterator();
//		while(it.hasNext()){
//			JsonElement e = (JsonElement)it.next();
//			//JsonElement转换为JavaBean对象
//			product = gson.fromJson(e, Product.class);
//	
//			System.out.println("Name:" + product.getFrom());
//			System.out.println("Id:" + product.getTo());
//			
//		}	
//		System.out.println();
	}
}
package com.penngo.word;

import java.io.*;  
import com.aspose.words.*;
public class WordToPdf {
	public void word(String Address,String to) {
		
		 try {
	    long old = System.currentTimeMillis();
	                File file = new File(to);  //新建一个空白pdf文档
	                 FileOutputStream os = new FileOutputStream(file);
	                 Document doc = new Document(Address);                    //Address是将要被转化的word文档
	                 doc.save(os, SaveFormat.PDF);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
	                 long now = System.currentTimeMillis();
	                 System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒");  //转化用时
	                 os.close();
	             } catch (Exception e) {
	                 e.printStackTrace();
	             }
		
	}
	
}

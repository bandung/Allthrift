# -*- coding:utf-8 -*-  
import sys
sys.path.append('..')

from thrift.TMultiplexedProcessor import TMultiplexedProcessor
from thrift.protocol.TMultiplexedProtocol import TMultiplexedProtocol
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
#根据实际的包结构去引入
import FaceService



    

def client():
    transport = TSocket.TSocket(host='localhost', port=8848) 
    transport = TTransport.TBufferedTransport(transport) 
    protocol = TBinaryProtocol.TBinaryProtocol(transport) 
    face_protocol = TMultiplexedProtocol(protocol, "FaceService") #如果服务端使用TMultiplexedProcessor接收处理，客户端必须用TMultiplexedProtocol并且指定serviceName和服务端的一致 
        
    face_client = FaceService.Client(face_protocol)#msg客户端 
        
    transport.open() 
    #打开链接 
    print face_client.getFace("123","啊实打实多")
        
    transport.close()


 
if __name__ == '__main__':
    client()
    
# -*- coding:utf-8 -*-  
import sys
sys.path.append('..')

from thrift.TMultiplexedProcessor import TMultiplexedProcessor
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
#根据实际的包结构去引入
import FaceService

#face.thrift的具体实现
class FaceServiceImpl:
    def __init__(self):
        self.log = {}
 
    def getFace(self,query, params):
        print "有连接了!"
        return query + (params)+"Python 伟大"

def TS():
    transport = TSocket.TServerSocket(host='0.0.0.0',port=8848)

    handler = FaceServiceImpl()
    processor = FaceService.Processor(handler)
    
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()
 
    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
    print 'python server:ready to start'
    server.serve()

def TM():
    transport = TSocket.TServerSocket(host='0.0.0.0',port=8848) 
    
    face_processor = FaceService.Processor(FaceServiceImpl()) #定义msg处理器
    
    tfactory = TTransport.TBufferedTransportFactory() 
    pfactory = TBinaryProtocol.TBinaryProtocolFactory() 
    processor = TMultiplexedProcessor() #使用TMultiplexedProcessor接收多个处理 
    processor.registerProcessor("FaceService", face_processor) #注册msg服务 
    
    
    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory) 
    server.serve() #开始监听请求


 
if __name__ == '__main__':
    TM()
    
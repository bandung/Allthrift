﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thrift.Protocol;
using Thrift.Server;
using Thrift.Transport;

namespace gen_csharp{ 

    class Server
    {

        //服务端注册多个service 多线程服务端
        public  void Run()
        {
            TServerSocket serverTransport = new TServerSocket(8848, 0, false);
            FaceService.Processor processor = new FaceService.Processor(new FaceServiceImpl());


            TMultiplexedProcessor tprocessor = new TMultiplexedProcessor();

            tprocessor.RegisterProcessor("FaceService", processor);

            TServer server = new TSimpleServer(tprocessor, serverTransport);
            
            Console.WriteLine("Starting server on port 7911 ...");
            server.Serve(); 

        }
        //单线程服务端
        public void run2()
        {

            TServerSocket serverTransport = new TServerSocket(8848, 0, false);
            FaceService.Processor processor = new FaceService.Processor(new FaceServiceImpl());
            TServer server = new TSimpleServer(processor, serverTransport);
            Console.WriteLine("Starting server on port 7911 ...");
            server.Serve(); 

        }

        //单线程客户端
        public void client()
        {
             TTransport transport = new TSocket("localhost", 8848); 
            TProtocol protocol = new TBinaryProtocol(transport); 
            FaceService.Client client = new FaceService.Client(protocol); 
            transport.Open(); 
            Console.WriteLine("Client calls .....");


            String info=client.getFace("123", "武切维奇");

            Console.WriteLine(info);
      
            
            transport.Close();

            Console.ReadKey(); 
        }
        //调用多线程的客户端
        public void moreClient()
        {

            TTransport trans = new TSocket("127.0.0.1",8080);
            trans.Open();
           

            TProtocol Protocol = new TBinaryProtocol(trans, true, true);
            TMultiplexedProtocol multiplex = new TMultiplexedProtocol(Protocol, "FaceService");
            FaceService.Iface client = new FaceService.Client(multiplex);
          
            String info= client.getFace("123","阿斯达");

            Console.WriteLine(info);

            trans.Close();
        }

    }
}

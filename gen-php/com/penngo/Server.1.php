<?php
namespace com\penngo;

require_once __DIR__.'/../../php/lib/Thrift/ClassLoader/ThriftClassLoader.php';

use Thrift\ClassLoader\ThriftClassLoader;

$GEN_DIR = realpath(dirname(__FILE__)).'/../../../gen-php';



$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', __DIR__ . '/../../php/lib');

$loader->registerDefinition('com', $GEN_DIR);

$loader->register();

if (php_sapi_name() == 'cli') {
    ini_set("display_errors", "stderr");
}

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Protocol\TMultiplexedProtocol;
use Thrift\Protocol\TProtocol;
use Thrift\Transport\TSocket;
use Thrift\Transport\THttpClient;
use Thrift\Transport\TBufferedTransport;
use Thrift\Exception\TException;
use com\penngo\RegisterServiceClient;
use com\penngo\LoginServiceClient;
use com\penngo\FaceServiceClient;

try {

    $socket = new TSocket('127.0.0.1', 8848);
    $socket->setSendTimeout(100000);
    $socket->setRecvTimeout(100000);


    $protocol = new TBinaryProtocol($socket);



    $faceClient = new FaceServiceClient($protocol);

    $socket->open();

    $user = $faceClient->getFace("123","asdasd");

    print "收到C#返回值".$user;
    $socket->close();

} catch (TException $tx) {
    print 'TException: '.$tx->getMessage()."\n";
    print 'TException: '.$tx->getTraceAsString()."\n";
}
<?php
namespace com\penngo;

require_once __DIR__.'/../../php/lib/Thrift/ClassLoader/ThriftClassLoader.php';
//echo __DIR__.'/../../lib/Thrift/ClassLoader/ThriftClassLoader.php';
use Thrift\ClassLoader\ThriftClassLoader;

$GEN_DIR = realpath(dirname(__FILE__)).'/../../../gen-php';



$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', __DIR__ . '/../../php/lib');
//$loader->registerDefinition('shared', $GEN_DIR);
$loader->registerDefinition('com', $GEN_DIR);
$loader->register();

if (php_sapi_name() == 'cli') {
    ini_set("display_errors", "stderr");
}

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Protocol\TMultiplexedProtocol;
use Thrift\Transport\TSocket;
use Thrift\Transport\THttpClient;
use Thrift\Transport\TBufferedTransport;
use Thrift\Exception\TException;
use com\penngo\RegisterServiceClient;
use com\penngo\LoginServiceClient;

try {

    $socket = new TSocket('127.0.0.1', 8848);
    $socket->setSendTimeout(100000);
    $socket->setRecvTimeout(100000);

    $transport = new TBufferedTransport($socket, 1024, 1024);
    $protocol = new TBinaryProtocol($transport);
//    $loginProtocol = new TMultiplexedProtocol($protocol, "LoginService");
    $faceProtocol = new TMultiplexedProtocol($protocol, "FaceService");
    // $registerProtocol = new TMultiplexedProtocol($protocol, "RegisterService");
    $faceClient = new FaceServiceClient($faceProtocol);
    // $registerClient = new RegisterServiceClient($registerProtocol);
    $transport->open();

    $faceinfo = $faceClient->getFace("123","asdasd");

    print_r($faceinfo);

    $transport->close();
} catch (TException $tx) {
    print 'TException: '.$tx->getMessage()."\n";
    print 'TException: '.$tx->getTraceAsString()."\n";
}
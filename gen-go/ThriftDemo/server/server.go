package main

import (
	"ThriftDemo/com/penngo"
	"fmt"
	"git.apache.org/thrift.git/lib/go/thrift"
	"log"
	"strings"
)

type PhpImpl struct {}

func (fdi *PhpImpl) Getphp(data *penngo.Message) (r *penngo.Message, err error){
	var rData penngo.Message
	fmt.Println("得到参数",data.String())
	rData.Text = strings.ToUpper(data.Text)

	return &rData, nil
}

type FaceImpl struct {}

func (fdi *FaceImpl)GetFace(name string, psw string) (r string, err error){
	var rData string
	fmt.Println("得到参数",name,psw)
	rData=name
	return rData, nil
}

const (
	HOST = "127.0.0.1"
	PORT = "8080"
)

func server1(){
	handler := &PhpImpl{}
	processor := penngo.NewPhpServiceProcessor(handler)
	//processor := penngo.NewPhpServiceProcessor(handler)
	serverTransport, err := thrift.NewTServerSocket(HOST + ":" + PORT)
	if err != nil {
		log.Fatalln("Error:", err)
	}
	//transportFactory := thrift.NewTFramedTransportFactory(thrift.NewTTransportFactory())
	//protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()

	//TMultiplexedProcessor
	tprocessor :=thrift.NewTMultiplexedProcessor()
	tprocessor.RegisterProcessor("PHPService", processor);

	//TSimpleServer
	server := thrift.NewTSimpleServer2(tprocessor, serverTransport)
	fmt.Println("Running at:", HOST + ":" + PORT)
	server.Serve()
}

func main() {
	handler := &FaceImpl{}
	processor := penngo.NewFaceServiceProcessor(handler)
	//processor := penngo.NewPhpServiceProcessor(handler)
	serverTransport, err := thrift.NewTServerSocket(HOST + ":" + PORT)
	if err != nil {
		log.Fatalln("Error:", err)
	}

	tprocessor :=thrift.NewTMultiplexedProcessor()
	tprocessor.RegisterProcessor("FaceService", processor);

	server := thrift.NewTSimpleServer2(tprocessor, serverTransport)
	fmt.Println("Running at:", HOST + ":" + PORT)
	server.Serve()

}
namespace java com.penngo
namespace php com.penngo
namespace py com.penngo
namespace go com.penngo
struct User {
	1: i64 id,
	2: string name,
	3: string password
}
struct Message {
    1: string text
}
 
service LoginService{
    User login(1:string name, 2:string psw);
} 

service FaceService{
    string getFace(1:string name, 2:string psw);
}
 
service RegisterService{
    User createUser(1:string name, 2:string psw);
}

service PHPService {
    Message getphp(1:Message message),
}